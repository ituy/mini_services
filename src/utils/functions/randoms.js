import dataName from './../../data/names'


const randomNumberBetween = async (start, end) => { // min and max included 
    return Math.floor(Math.random() * (end - start + 1) + start)
}


const randomStringLength = (length) => {
    let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    let result = ''
    for (let i = 0; i < length; i++) {
        result += chars.charAt(Math.floor(Math.random() * chars.length));
    }

    return result;
}


const randomFullname = async (listName = null, idx = null) => {
    const names = listName || await dataName.getNamesList()
    const indexRand = idx || await randomNumberBetween(0, names.length)

    return names[indexRand]
}


export default {
    randomNumberBetween,
    randomStringLength,
    randomFullname
}
