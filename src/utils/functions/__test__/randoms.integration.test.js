import dataNames from './../../../data/names'
import rand from './../randoms'


const mock_getNamesList = async () => {
    return [
        'Maire Capel',
        'Ashely Housand',
        'Eleonor Sesco',
        'Gerardo Biggers',
        'Shanon Rosati',
        'Genevive Oldham',
        'Morgan Bevacqua',
        'Steve Howle',
        'Jenny Brenner'
    ]
}


const mock_randomNumberBetween = async () => {
    return 4
}



test('Integation test : get data fullname and random fullname from data fullname', async () => {
    
    // let names = await mock_dataNames.mock_getNamesList()
    // let indexRand = await mock_randomNumberBetween()
    let names = await dataNames.getNamesList()
    let indexRand = await rand.randomNumberBetween(0, names.length)

    expect(names).not.toBeNull()
    expect(indexRand).not.toBeNaN()
    expect(rand.randomFullname(names, indexRand)).not.toBe("")
    expect(rand.randomFullname(names, indexRand)).not.toBeNull()
    expect(rand.randomFullname(names, indexRand)).not.toBeNaN()

})
